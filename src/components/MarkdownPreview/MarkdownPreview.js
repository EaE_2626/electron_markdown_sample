import React from 'react';
import { connect } from 'react-redux';
import remark from 'remark';
import reactRenderer from 'remark-react';
// material-ui
import { makeStyles } from '@material-ui/core';

const mapStateToProps = state => ({
  text: state.mkEditor.text
})

const useStyle = makeStyles(() => ({
  textbox: {
    width: 200,
    height: 200
  }
}))

function MarkdownPreview({ text }) {
  const classes = useStyle()

  return(
    <div className={classes.textbox}>
      {remark().use(reactRenderer, { sanitize: false }).processSync(text).contents}
    </div>
  )
}

export default connect(mapStateToProps)(MarkdownPreview)