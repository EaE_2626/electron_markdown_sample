import { EditorState } from 'draft-js';

// Action 
const EDIT_ACTION = 'EDIT_ACTION'

// Action Creater
export const editAction = editorState => ({
  type: EDIT_ACTION,
  payload: {
      editorState,
      text: editorState.getCurrentContent().getPlainText()
  }
})

// Reducer
const initialState = {
  editorState: EditorState.createEmpty(),
  text: ''
}

export function mkEditorReducer(state = initialState, action) {
  switch(action.type) {
    case EDIT_ACTION: 
      return {
        ...state,
        editorState: action.payload.editorState,
        text: action.payload.text
      }
    default:
      return state
  }
}

