import React from 'react';
import { connect } from 'react-redux';
// draft-js
import { Editor } from 'draft-js';
// module
import { editAction } from './module';

const mapStateToProps = state => ({
  editorState: state.mkEditor.editorState,
  text: state.mkEditor.text
})

const mapDispatchToProps = dispatch => ({
  onChangeAction(editorState) {
    dispatch(editAction(editorState))
  }
})

function MarkdownEditor({ editorState, onChangeAction}) {  
  return(
    <Editor
      editorState={editorState}
      onChange={onChangeAction}
    />
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(MarkdownEditor)