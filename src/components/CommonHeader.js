import React from 'react';
import PropTypes from 'prop-types';
// material-ui 
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/ToolBar';
import Typography from '@material-ui/core/Typography';

function CommonHeader({ apptitle }) {
  return(
    <AppBar position="static" color="primary">
      <ToolBar>
        <Typography varinant="h4">
          {apptitle}
        </Typography>
      </ToolBar>
    </AppBar>
  )
}

CommonHeader.propTypes = {
  apptitle: PropTypes.string.isRequired
}

export default CommonHeader

