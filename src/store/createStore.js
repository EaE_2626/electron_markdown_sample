import { createStore as CreateReduxStore, combineReducers } from 'redux';
// reducer
import { mkEditorReducer } from '../components/MarkdownEditor/module';

export const createStore = () => {
  const combReducer = combineReducers({
    mkEditor: mkEditorReducer
  });

  return CreateReduxStore(
    combReducer
  )
}