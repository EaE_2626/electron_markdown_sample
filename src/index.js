import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
// components
import App from './App';
// store
import { createStore } from './store/createStore';

const store = createStore()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'))