import React from 'react';
// material-ui
import { Grid, Card, CardContent, makeStyles } from '@material-ui/core';
// componetns
import CommonHeader from './components/CommonHeader';
import MarkdownEditor from './components/MarkdownEditor/MarkdownEditor';
import MarkdownPreview from './components/MarkdownPreview/MarkdownPreview';


const useStyles = makeStyles(() => ({
  card: {
    margin: 40,
    width: 300,
    height: 400,
    maxWidth: 300,
    maxHeight: 400
  }
}))

function App() {
  const classes = useStyles()

  return(
    <React.Fragment>
    <CommonHeader apptitle="Electron Sample"/>
      <Grid container justify="center">
        <Grid item>
          <Card className={classes.card}>
            <CardContent>
              <MarkdownEditor />
            </CardContent>
          </Card>
        </Grid>
        <Grid item>
          <Card className={classes.card}>
            <CardContent>
              <MarkdownPreview />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default App